/*
   Nama Program : Mencetak bintang
   Tgl buat     : 20 November 2023
   Deskripsi    : Soal UTS
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int N; 

    cout << "Masukkan N = ";
    cin >> N;

    for (int i = 0; i <= N; i++) 
    {
        for (int j = N; j > i; j--) 
        {
            cout << " ";
        }
        for (int k = 0; k < i; k++)
        {
            cout << " *";
        }
            cout << endl;
        }
    for (int i = 1; i < N; i++)
    {
        for (int j = 0; j < i; j++)
        {
            cout << " ";
        }
        for (int k = N; k > i; k--)
        {
            cout << " *";
        }
        cout << endl;
    }
    return 0;
    }
