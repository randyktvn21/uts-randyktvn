/*
   Nama         : Randy Oktaviana Hertland
   NIM          : 301230053
   Tgl buat     : 6 Desember 2023
   Deskripsi    : kuis segitiga pascal
*/
#include <stdio.h>

unsigned long long faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

unsigned long long kombinasi(int n, int r) {
    if (r > n) {
        return 0; 
    } else {
        return faktorial(n) / (faktorial(r) * faktorial(n - r));
    }
}

int main() {
    int tingkat;

    printf("Masukkan tingkat segitiga Pascal: ");
    scanf("%d", &tingkat);

    for (int i = 0; i < tingkat; i++) {

        for (int j = 0; j < tingkat - i - 1; j++) {
            printf(" ");
        }

        for (int j = 0; j <= i; j++) {
            printf("%llu ", kombinasi(i, j));
        }

        printf("\n");
    }

    return 0;
}
